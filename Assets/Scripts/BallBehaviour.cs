﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviour : MonoBehaviour
{
    public Vector2 speed = new Vector2(5, 5);

    public float posX = 0;
    public float posY = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        posX += speed.x * Time.deltaTime;
        posY += speed.y * Time.deltaTime;

        if (posX >= 8.3 || posX <= -8.3)
        {
            speed.x *= -1;
        }

        if (posY >= 5.5 || posY <= -3.5f)
        {
            speed.y *= -1;
        }

        transform.position = new Vector3(posX, posY, 0f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "EnemyH")
        {
            speed.x *= -1;
        }

        if (collision.gameObject.name == "EnemyV")
        {
            speed.y *= -1;
        }
    }
}
