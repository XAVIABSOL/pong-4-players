﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public enum EnemyPos { DOWN, UP, LEFT, RIGTH }
    public EnemyPos enemyPos;

    public BallBehaviour ball;

    float posY = 0;
    float posX = 0;
    public float speed = 5f;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        switch(enemyPos)
        {
            case EnemyPos.DOWN:
                {
                    if (ball.posY < 0)
                    {
                        FollowBallX();
                    }
                    break;
                }
            case EnemyPos.UP:
                {
                    if (ball.posY > 0)
                    {
                        FollowBallX();
                    }
                    break;
                }
            case EnemyPos.RIGTH:
                {
                    if (ball.posX > 0)
                    {
                        FollowBallY();
                    }
                    break;
                }
            case EnemyPos.LEFT:
                {
                    if (ball.posX < 0)
                    {
                        FollowBallY();
                    }

                    break;
                }
        }

    }

    void FollowBallX()
    {
        if (ball.posX > posX)
        {
            posX += speed * Time.deltaTime;
        }
        else if (ball.posX < posX)
        {
            posX -= speed * Time.deltaTime;
        }

        transform.position = new Vector3(posX, transform.position.y, 0);
    }

    void FollowBallY()
    {
        if (ball.posY > posY)
        {
            posY += speed * Time.deltaTime;
        }
        else if (ball.posY < posY)
        {
            posY -= speed * Time.deltaTime;
        }

        transform.position = new Vector3(transform.position.x, posY, 0);
    }
}
