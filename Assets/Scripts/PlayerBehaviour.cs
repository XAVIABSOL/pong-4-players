﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{

    private float speed = 10;
    public float posY = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow) && posY <= 5.5f)
        {
            posY += speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.DownArrow) && posY >= -3.5f)
        {
            posY -= speed * Time.deltaTime;
        }

        transform.position = new Vector3(transform.position.x, posY, 0f);
    }
}
